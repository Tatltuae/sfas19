﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    RectTransform image;
    RhythmPlayer player;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<RectTransform>();
        player = FindObjectOfType<RhythmPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        image.localScale = new Vector3(player.Health / player.MaxHealth, image.localScale.y, image.localScale.z);
    }
}
