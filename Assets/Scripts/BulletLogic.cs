﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLogic : MonoBehaviour
{
    // The lifetime of the bullet
    [SerializeField]
    float m_BulletLifeTime = 2.0f;

    // The speed of the bullet
    [SerializeField]
    protected float m_BulletSpeed = 15.0f;

    // The damage of the bullet
    [SerializeField]
    int m_Damage = 20;

    bool m_Active = true;

    FightPosition fightPosition = FightPosition.NONE;

    // Use this for initialization
    void Start()
    {
        // Add velocity to the bullet
        GetComponent<Rigidbody>().velocity = -transform.up * m_BulletSpeed;
        GameEvents.OnCombatInitiate += OnCombatStart;
        GameEvents.OnCombatEnd += OnCombatEnd;
    }

    // Update is called once per frame
    void Update ()
    {
        m_BulletLifeTime -= MyTime.DeltaTime(fightPosition);
        if(m_BulletLifeTime < 0.0f)
        {
            Impact();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(!m_Active)
        {
            return;
        }

        if (collision.collider.tag == "Player" && collision.collider.name == "RhythmPlayer")
        {
            collision.collider.GetComponent<RhythmPlayer>().DamageHealth(10.0f);
        }

        Impact();
        m_Active = false;
    }

    void Impact()
    {
        GameEvents.OnCombatInitiate -= OnCombatStart;
        GameEvents.OnCombatEnd -= OnCombatEnd;
        Explode();
        Destroy(gameObject);
    }

    protected virtual void Explode()
    {
    }

    private void OnCombatStart(RhythmPlayer player, RhythmEnemy enemy)
    {
        fightPosition = FightPosition.BYSTANDER;
        // Remove velocity from the bullet
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    private void OnCombatEnd()
    {
        fightPosition = FightPosition.NONE;
        // Add velocity to the bullet
        GetComponent<Rigidbody>().velocity = -transform.up * m_BulletSpeed;
    }
}
