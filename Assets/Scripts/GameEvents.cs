﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public delegate void PlayerDied();
    public static PlayerDied PlayerHasDied;

    public delegate void CombatInitiated(RhythmPlayer player, RhythmEnemy enemy);
    public static CombatInitiated OnCombatInitiate;

    public delegate void CombatEnd();
    public static CombatEnd OnCombatEnd;
}
