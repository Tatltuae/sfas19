﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RhythmManager : MonoBehaviour
{
    [SerializeField]
    public AudioClip[] clips = new AudioClip[2];
    [SerializeField]
    private RhythmColumn[] columns = new RhythmColumn[4];
    [SerializeField]
    private RectTransform[] bars = new RectTransform[3];
    [SerializeField]
    private RectTransform spawnMinimum;
    [SerializeField]
    private List<RectTransform> beats;

    private RhythmSongData song = new RhythmSongData();
    private RhythmPlayer player;
    private RhythmEnemy enemy;

    private BackgroundChords bgchords;
    private float timeOfChordChange = 1.0f;
    private AudioSource source;
    public float[] currentNotes = new float[] { };
    private float timeOfNextNote = 1.0f;

    private float timeOfSongStart;
    private int spawnBeatIndex = 0;
    private int currentBeat = 0;
    private Timer beatCD = new Timer();
    private Timer startCombatCD = new Timer();
    private Timer endSongTimer = new Timer();

    private int bpm = 100;
    private float speed;
    private float beatTime;
    private float beatSeparateY;

    private bool[] axisReleased = new bool[] { true, true, true, true };
    private string[] axisName = new string[] { "Column1", "Column2", "Column3", "Column4" };

    private bool playing = false;

    public RectTransform SpawnBeat;

    // Start is called before the first frame update
    void Start()
    {
        // Get distance between beats
        beatSeparateY = beats[1].anchoredPosition.y - beats[0].anchoredPosition.y;
        // Establish scroll speed
        beatTime = 60.0f / bpm;
        speed = beatSeparateY / beatTime;
        // Add values to song
        song.CreateDefault();
        bgchords = GetComponentInChildren<BackgroundChords>();
        bgchords.Initialise(EnemyType.BOSS);
        source = GetComponent<AudioSource>();

        player = FindObjectOfType<RhythmPlayer>();

        ResetSpawnBeat();

        GameEvents.OnCombatInitiate += StartCombat;
        GameEvents.OnCombatEnd += ResetValuesAfter;
    }

    private void ResetSpawnBeat()
    {
        // Reset bar positions
        for (int i = 0; i < 3; i++)
        {
            bars[i].anchoredPosition =
                new Vector2(bars[i].anchoredPosition.x, bars[i].sizeDelta.y * i + 200);
        }
        // Find first spawn beat
        spawnBeatIndex = 0;
        while (beats[spawnBeatIndex].transform.position.y < spawnMinimum.position.y)
        {
            NextSpawnBeat(true);
        }
        spawnBeatIndex--;
        if (spawnBeatIndex < 0)
        {
            spawnBeatIndex = beats.Count - 1;
        }
        SpawnBeat = beats[spawnBeatIndex];
    }

    private void ResetValuesBefore()
    {
        // Period of time to show the ui before the song starts tracking
        float songStartDelay = 1.0f;
        startCombatCD.StartTimer(songStartDelay);
        // Get the system time at combat start
        timeOfSongStart = Time.time;
        timeOfNextNote = songStartDelay;
        timeOfChordChange = songStartDelay + (beatTime * 4.0f);
        // Set clip to metronome
        source.clip = clips[0];
        // Generate new song
        song.CreateDefault();
        //  Clear notes
        currentNotes = new float[] { };
        // Zero beat values
        ResetSpawnBeat();
        currentBeat = 0;
        // Reset all button hold states
        axisReleased = new bool[] { true, true, true, true };
        // Start song end timer
        endSongTimer.StartTimer(beatTime * 53.0f + songStartDelay);
    }

    private void ResetValuesAfter()
    {
        // Place the ui off screen
        RectTransform trans = GetComponent<RectTransform>();
        trans.anchoredPosition = new Vector2(-700, trans.anchoredPosition.y);
        // Stop playing 
        playing = false;
        if(enemy.Health <= 0)
        {
            Destroy(enemy.gameObject);
        }
        enemy = null;
    }

    public void StartCombat(RhythmPlayer player, RhythmEnemy enemy)
    {
        this.enemy = enemy;
        // Reset values
        ResetValuesBefore();
        // Place the ui on screen
        RectTransform trans = GetComponent<RectTransform>();
        trans.anchoredPosition = new Vector2(0, trans.anchoredPosition.y);
        // Set playing to true
        playing = true;
        // Make a new song
        bgchords.Initialise(enemy.Type);
        // ~~ scroll in
        // ~~ play four ticks
        // ~~ swap back to clips [1]
        // Start combat timer
    }

    void UpdateTimers(float dt)
    {
        beatCD.PassTime(dt);
        startCombatCD.PassTime(dt);
        endSongTimer.PassTime(dt);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Update timers
        UpdateTimers(Time.fixedDeltaTime);

        // If song isn't playing avoid updates
        if (!playing || !startCombatCD.IsDone)
        {
            return;
        }

        // End the song when out of chords and timer exhausted
        if (endSongTimer.IsDone)
        {
            GameEvents.OnCombatEnd();
        }

        // On each beat
        if (Time.time - timeOfSongStart >= timeOfNextNote)
        {
            // If pre-chords play metronome until chords
            if (currentBeat < 5)
            {
                source.Stop();
                source.pitch = 1.0f;
                source.Play();
            }
            else
            {
                // If at the start of a bar, chord change
                if ((currentBeat - 1) % 4 == 0)
                {
                    currentNotes = bgchords.NextChord();
                    timeOfChordChange += 4.0f * beatTime;
                }
            }
            timeOfNextNote += beatTime;
        }

        // Check input axis for column hits
        for (int i = 0; i < 4; ++i)
        {
            if (axisReleased[i] && Input.GetAxisRaw(axisName[i]) == 1)
            {
                axisReleased[i] = false;

                // Check if input hit
                Accuracy accuracy = columns[i].CheckHit();
                float multiplier = 0.0f;
                switch (accuracy)
                {
                    case Accuracy.PERFECT:
                        multiplier = 1.0f;
                        break;
                    case Accuracy.GOOD:
                        multiplier = 0.65f;
                        break;
                    case Accuracy.OK:
                        multiplier = 0.3f;
                        break;
                }
                // If left two columns attack enemy
                if (i <= 1)
                {
                    enemy.DamageHealth(multiplier * player.Damage / 10.0f);
                }
                // if right two columns attack player
                else
                {
                    // (1 - multiplier) so a perfect block multiplies damage by 0
                    player.DamageHealth((1.0f - multiplier) * enemy.Damage / 10.0f);
                }
            }
            else if (!axisReleased[i] && Input.GetAxisRaw(axisName[i]) == 0)
            {
                axisReleased[i] = true;
            }
        }

        // Update bars
        for (int i = 0; i < 3; i++)
        {
            bars[i].anchoredPosition += (Vector2.down * Time.fixedDeltaTime * speed);
            // Jump bar up if far down
            if (bars[i].anchoredPosition.y < -950)
            {
                bars[i].anchoredPosition += Vector2.up * 3 * 1200;
            }
        }

        // Update spawn beat
        if (!song.SongFinished() && beatCD.IsDone && beats[spawnBeatIndex].transform.position.y < spawnMinimum.position.y)
        {
            NextSpawnBeat(false);
        }
    }

    private void NextSpawnBeat(bool limited)
    {
        spawnBeatIndex++;
        spawnBeatIndex %= beats.Count;
        SpawnBeat = beats[spawnBeatIndex];
        if (!limited)
        {
            if (song.NextNeededBeat() == currentBeat)
            {
                PopulateBeat();
            }
            NextCurrentBeat();
        }
    }

    private void PopulateBeat()
    {
        List<KeyValuePair<int, float>> notes = song.UseNextBeatInfo();
        foreach (KeyValuePair<int, float> pair in notes)
        {
            columns[pair.Key].StartMarker(speed, (pair.Value - currentBeat) * (beats[1].position.y - beats[0].position.y));
        }
    }

    private void NextCurrentBeat()
    {
        currentBeat++;
    }
}
