﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RhythmSongData
{
    private Queue<float>[] columnQueues = new Queue<float>[4];
    private int mostRecentBeat = -1;
    private bool[] finished = new bool[] { false, false, false, false };

    public RhythmSongData()
    {
        for (int i = 0; i < 4; i++)
        {
            columnQueues[i] = new Queue<float>();
        }
    }

    public bool SongFinished() { return finished[0] && finished[1] && finished[2] && finished[3]; }

    private void Reset()
    {
        for (int i = 0; i < 4; i++)
        {
            // Empty queues
            columnQueues[i].Clear();
            // Set all unfinished
            finished[i] = false;
        }
        // Reset current beat
        mostRecentBeat = -1;
    }

    public void CreateDefault()
    {
        Reset();
        for (int i = 0; i < 45; i++)
        {
            // Beat at i with 10% chance to be off-beat
            float beat = i + (Random.Range(0.0f, 1.0f) < 0.1f ? 0.5f : 0.0f);
            // Note column ranging from 0 to 3
            int note1 = Random.Range(0, 4);
            // If last note
            if (i == 44)
            {
                // No off-beats
                beat = i;
                // End on root note
                note1 = 0;
            }
            // Add the note
            AddNote(note1, beat);
            // Add a second note at 10% chance
            if (Random.Range(0.0f, 1.0f) < 0.1)
            {
                // Reroll note 2 until it is different to the first
                int note2 = Random.Range(0, 4);
                while (note2 == note1)
                {
                    note2 = Random.Range(0, 4);
                }
                // Add note 2 alongside note 1
                AddNote(note2, beat);
            }
        }
    }

    // Adds a note to the appropriate column at a given beat time
    public void AddNote(int column, float beat)
    {
        columnQueues[column].Enqueue(beat);
    }

    // Return the next beat with notes
    public int NextNeededBeat()
    {
        if (SongFinished()) { return -1; }
        // Output variable that tracks soonest note
        int i = 0;
        int lowest = -1;
        // Find first column with a float value to check against
        for (i = 0; i < 4; i++)
        {
            if (columnQueues[i].Count > 0)
            {
                lowest = (int)columnQueues[i].Peek();
                break;
            }
        }
        // Find the column with the earliest note
        for (i = i + 1; i < 4; i++)
        {
            if (columnQueues[i].Count == 0)
            {
                continue;
            }
            if ((int)columnQueues[i].Peek() < lowest)
            {
                lowest = (int)columnQueues[i].Peek();
            }
        }
        // If the value found is too early clean the queues and re-find lowest
        if (lowest <= mostRecentBeat && lowest != -1)
        {
            ClearBadNotes();
            lowest = NextNeededBeat();
        }
        return lowest;
    }

    // Removes notes from the from of queues that are too late
    private void ClearBadNotes()
    {
        for (int i = 0; i < 4; i++)
        {
            if (columnQueues[i].Count == 0)
            {
                continue;
            }
            // Discard all notes that would exist in the past
            while ((int)columnQueues[i].Peek() <= mostRecentBeat)
            {
                columnQueues[i].Dequeue();
            }
        }
    }

    // Return the columns and notes of the next beat
    public List<KeyValuePair<int, float>> UseNextBeatInfo()
    {
        int beat = NextNeededBeat();
        List<KeyValuePair<int, float>> outList = new List<KeyValuePair<int, float>>();
        // For each column add note details for next beat.  
        for (int i = 0; i < 4; i++)
        {
            if (columnQueues[i].Count == 0)
            {
                continue;
            }
            // While this column has relevant notes add them to the output list, and dequeue them
            // Queue is popped until the values stop matching the appropriate beat value
            while (beat == (int)columnQueues[i].Peek())
            {
                outList.Add(new KeyValuePair<int, float>(i, columnQueues[i].Dequeue()));
                if (columnQueues[i].Count == 0)
                {
                    break;
                }
            }
        }
        // Set mostRecentBeat for future error checking
        mostRecentBeat = beat;
        return outList;
    }
}
