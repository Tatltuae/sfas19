﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer
{
    private float duration = 0.0f;
    private float remaining = 0.0f;
    private bool done = true;

    // Start the timer
    public bool StartTimer(float length)
    {
        // If not timing anything set values and return true
        if (done)
        {
            duration = length;
            remaining = length;
            done = false;
            return true;
        }
        return false;
    }

    public bool RestartTimer()
    {
        return StartTimer(TotalDuration);
    }
    // Decrement remaining time by time value
    public void PassTime(float time)
    {
        if (!done)
        {
            remaining -= time;
            if (remaining <= 0)
            {
                done = true;
            }
        }
    }
    // Check remaining time
    public float Remaining
    {
        get { return remaining; }
    }
    // Check total duration
    public float TotalDuration
    {
        get { return duration; }
    }
    // Check timer is finished
    public bool IsDone
    {
        get { return done; }
    }
    // Return progress as fraction
    public float Progress()
    {
        return 1.0f - (remaining / duration);
    }
}
