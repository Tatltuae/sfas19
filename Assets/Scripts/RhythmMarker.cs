﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Accuracy { PERFECT, GOOD, OK, FAIL, NONE };

public class RhythmMarker : MonoBehaviour
{
    [SerializeField]
    private Transform top;
    [SerializeField]
    private Transform bottom;

    private RectTransform rectTransform;
    // Marker available for use if false
    private bool isMoving = false;
    private float speed;
    // Time marker can be from base to get different accuracies
    private static float[] timeCutOff = new float[]{0.05f, 0.1f, 0.2f, 0.4f};

    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        GameEvents.OnCombatEnd += Reset;
    }

    void Reset()
    {
        isMoving = false;
        transform.position = top.position;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        // If marker is currently active advance movement
        if (isMoving)
        {
            rectTransform.anchoredPosition += (Vector2.down * Time.fixedDeltaTime * speed);
            // If marker reaches bottom of screen end descent
            if (transform.position.y < bottom.position.y)
            {
                EndDescent();
            }
        }
    }

    // Start the marker moving downwards
    public void StartDescent(float duration)
    {
        // Set marker to start position at top 
        transform.position = top.position;
        // Calculate speed as distance/time
        // speed = top.position.y / duration; --------------
        speed = duration;
        // Set moving bool
        isMoving = true;
    }

    // Take care of marker when descent is over
    private void EndDescent()
    {
        transform.position = bottom.position;
        isMoving = false;
    }

    // Check how accurately the person pressed the appropriate input
    public Accuracy CheckHit()
    {
        // Convert remaining distance to time for accuracy comparison
        float distanceAsTime = Mathf.Abs(rectTransform.anchoredPosition.y) / speed;
        // Output value (initialised as fail, rather than none, if marker below base)
        //Accuracy returnAcc = (transform.localPosition.y > 0 ? Accuracy.NONE : Accuracy.FAIL);

        Accuracy returnAcc = Accuracy.NONE;

        // Check which threshold the hit falls under
        if (distanceAsTime <= timeCutOff[(int)Accuracy.PERFECT])
        {
            returnAcc = Accuracy.PERFECT;
        }
        else if(distanceAsTime <= timeCutOff[(int)Accuracy.GOOD])
        {
            returnAcc = Accuracy.GOOD;
        }
        else if (distanceAsTime <= timeCutOff[(int)Accuracy.OK])
        {
            returnAcc = Accuracy.OK;
        }
        else if (distanceAsTime <= timeCutOff[(int)Accuracy.FAIL])
        {
            returnAcc = Accuracy.FAIL;
        }
        // If the hit was within the fail time range end descent of marker
        if(returnAcc != Accuracy.NONE)
        {
            EndDescent();
        }

        return returnAcc;
    }

    public bool IsMoving
    {
        get { return isMoving; }
    }
}
