﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordLogic : MonoBehaviour
{
    private RhythmPlayer player;
    private Animator anim;
    // Whether the player is in rhythm combat
    bool inCombat = false;
    bool swinging = false;
    Timer swingCD = new Timer();

    FightPosition fightPosition = FightPosition.NONE;

    private bool Swinging
    {
        get { return swinging; }
        set
        {
            anim.SetBool("Swinging", value);
            swinging = value;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<RhythmPlayer>();
        anim = GetComponent<Animator>();
        GameEvents.OnCombatInitiate += OnCombatStart;
        GameEvents.OnCombatEnd += OnCombatEnd;
    }

    void UpdateTimers(float dt)
    {
        swingCD.PassTime(dt);
    }
    // Update is called once per frame
    void Update()
    {
        UpdateTimers(MyTime.DeltaTime(fightPosition));

        // Set swinging to false after a frame so animation won't loop accidentally
        if (swinging)
        {
            Swinging = false;
        }
        // Check for attack input
        if (!inCombat && swingCD.IsDone)
        {
            if (Input.GetAxisRaw("Fire1") > 0)
            {
                swingCD.StartTimer(1.5f);
                Swinging = true;
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (inCombat)
        {
            return;
        }
        // If sword hit an enemy
        if (other.tag == "Enemy")
        {
            GameEvents.OnCombatInitiate(player, other.GetComponent<RhythmEnemy>());
        }
    }

    private void OnCombatStart(RhythmPlayer player, RhythmEnemy enemy)
    {
        fightPosition = FightPosition.BYSTANDER;
    }

    private void OnCombatEnd()
    {
        fightPosition = FightPosition.NONE;
    }
}
