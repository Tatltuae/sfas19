﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RhythmColumn : MonoBehaviour
{
    [SerializeField]
    private Image baseImage;
    [SerializeField]
    private Transform top;

    private RhythmManager manager;

    [SerializeField]
    private int columnNumber;
    private AudioSource audio;

    private List<RhythmMarker> markers = new List<RhythmMarker>();
    private Queue<RhythmMarker> movers = new Queue<RhythmMarker>();

    private int missedMarkers = 0;
    private int totalMarkers = 0;

    // Start is called before the first frame update
    void Start()
    {
        // Add all child markers to this column's list of markers
        foreach (RhythmMarker marker in GetComponentsInChildren<RhythmMarker>())
        {
            markers.Add(marker);
        }
        // Find RhythmManager
        manager = FindObjectOfType<RhythmManager>();
        // Find audioSource
        audio = GetComponentInChildren<AudioSource>();
        GameEvents.OnCombatEnd += Reset;
    }

    private void Reset()
    {
        baseImage.color = Color.white;
        movers.Clear();
        missedMarkers = 0;
        totalMarkers = 0;
    }

    // Starts a marker descending. Returns false if none were available.
    public bool StartMarker(float duration, float yOffset)
    {
        // Marker to use
        RhythmMarker use = null;
        // Get first marker in list that's not moving
        foreach (RhythmMarker m in markers)
        {
            if (!m.IsMoving)
            {
                use = m;
                break;
            }
        }
        // If no usable marker was found return false
        if (use == null)
        {
            return false;
        }
        // Update position of top
        top.position = new Vector3(top.position.x, manager.SpawnBeat.position.y + yOffset, top.position.z);
        // Start chosen marker's descent
        use.StartDescent(duration);
        movers.Enqueue(use);
        totalMarkers++;
        return true;
    }

    // Play metronome tick
    private void PlayFail()
    {
        audio.Stop();
        audio.clip = manager.clips[0];
        audio.Play();
    }
    // Play note
    private void PlaySuccess()
    {
        audio.Stop();
        audio.clip = manager.clips[1];
        if (manager.currentNotes.Length > 0)
        {
            audio.pitch = manager.currentNotes[(columnNumber - 1) % manager.currentNotes.Length];
        }
        audio.Play();
    }
    // Check hit on this column
    public Accuracy CheckHit()
    {
        // While movers exist check for missed markers
        while (movers.Count > 0)
        {
            // If queue front was missed, remove it
            if (!movers.Peek().IsMoving)
            {
                missedMarkers++;
                movers.Dequeue();
            }
            // If queue front is still moving leave loop and check accuracy
            else
            {
                break;
            }
        }

        // Output variable
        Accuracy outAcc = Accuracy.NONE;
        // If there are moving markers, check front accuracy
        while (movers.Count > 0)
        {
            outAcc = movers.Peek().CheckHit();
            // If marker in hit range, remove it from queue
            if (outAcc != Accuracy.NONE)
            {
                movers.Dequeue();
            }
            // If accuracy none but marker below base, marker missed
            else if (movers.Peek().transform.localPosition.y < 0)
            {
                missedMarkers++;
                movers.Dequeue();
                baseImage.color = Color.grey;
                continue;
            }
            // Change base colour
            switch (outAcc)
            {
                case (Accuracy.NONE):
                    baseImage.color = Color.white;
                    break;
                case (Accuracy.FAIL):
                    baseImage.color = Color.black;
                    break;
                case (Accuracy.OK):
                    baseImage.color = Color.red;
                    break;
                case (Accuracy.GOOD):
                    baseImage.color = Color.yellow;
                    break;
                case (Accuracy.PERFECT):
                    baseImage.color = Color.green;
                    break;
                default:
                    baseImage.color = Color.white;
                    break;
            }
            // Exit loop if marker successfully hit
            break;
        }

        if (outAcc <= Accuracy.OK)
        {
            PlaySuccess();
        }
        else if (outAcc == Accuracy.FAIL)
        {
            PlayFail();
        }

        return outAcc;
    }
}
