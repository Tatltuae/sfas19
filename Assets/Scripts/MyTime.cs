﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FightPosition { NONE, BYSTANDER, FIGHTER };

public class MyTime : MonoBehaviour
{
    public static float DeltaTime(FightPosition position)
    {
        float multiplier = 1.0f;
        if(position == FightPosition.BYSTANDER)
        {
            multiplier = 0.0f;
        }
        return Time.deltaTime * multiplier;
    }

    public static float FixedDeltaTime(FightPosition position)
    {
        float multiplier = 1.0f;
        if (position == FightPosition.BYSTANDER)
        {
            multiplier = 0.0f;
        }
        return Time.fixedDeltaTime * multiplier;
    }
}
