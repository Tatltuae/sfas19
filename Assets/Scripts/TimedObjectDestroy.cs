﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedObjectDestroy : MonoBehaviour
{
    // The lifetime of the bullet
    [SerializeField]
    float m_Lifetime = 5.0f;

    FightPosition fightPosition = FightPosition.NONE;

    private void Start()
    {
        GameEvents.OnCombatInitiate += OnCombatStart;
        GameEvents.OnCombatEnd += OnCombatEnd;
    }

    // Update is called once per frame
    void Update ()
    {
        m_Lifetime -= MyTime.DeltaTime(fightPosition);
        if(m_Lifetime < 0.0f)
        {
            Destroy(gameObject);
        }
    }

    private void OnCombatStart(RhythmPlayer player, RhythmEnemy enemy)
    {
        fightPosition = FightPosition.BYSTANDER;
    }

    private void OnCombatEnd()
    {
        fightPosition = FightPosition.NONE;
    }
}
