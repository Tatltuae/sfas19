﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RhythmEnemy : MonoBehaviour
{
    [SerializeField]
    private float damage = 90.0f;
    [SerializeField]
    private float maxHealth = 40.0f;
    [SerializeField]
    private EnemyType type = EnemyType.REGULAR;

    private float currentHealth;


    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        GameEvents.OnCombatInitiate += CombatInitiated;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public float Damage { get { return damage; } }

    public float Health { get { return currentHealth; } }

    public EnemyType Type { get { return type; } }

    public void DamageHealth(float hit)
    {
        currentHealth -= hit;
        if (currentHealth < 0)
        {
            currentHealth = 0;
            // ~~ do death stuff
            // ~~ animate
        }
    }

    public void GiveHealth(float heal)
    {
        if (currentHealth + heal > maxHealth)
        {
            heal = maxHealth - currentHealth;
        }
        currentHealth += heal;
        // ~~ animate
    }

    public void CombatInitiated(RhythmPlayer player, RhythmEnemy enemy)
    {
        // If not this enemy return
        if(enemy != this)
        {
            return;
        }
        // ~~ combat enemy specific things
    }
}
