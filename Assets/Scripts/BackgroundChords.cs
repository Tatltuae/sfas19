﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType { REGULAR, BOSS };

public class BackgroundChords : MonoBehaviour
{
    private AudioSource[] audio;

    private int[][] pattern = new int[][] { new int[]{ 1, 2, 5, 3, 1, 2, 5, 3, 1, 4, 5, 1 }, // regular
        new int[]{ 6, 6, 2, 3, 6, 6, 2, 3, 2, 4, 5, 1 } }; // boss

    private int currentChordI = 0;

    EnemyType enemy;


    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponents<AudioSource>();
        GameEvents.OnCombatEnd += Reset;
    }

    private void Reset()
    {
        // Silence chords
        for(int i = 0; i < audio.Length; ++i)
        {
            audio[i].Stop();
        }
    }

    public bool ChordsFinished()
    {
        return currentChordI >= pattern[(int)enemy].Length;
    }

    public void Initialise(EnemyType type)
    {
        currentChordI = 0;
        enemy = type;
    }

    public float[] NextChord()
    {
        if (ChordsFinished())
        {
            for (int i = 0; i < 3; ++i)
            {
                audio[i].Stop();
            }
            return new float[] { };
        }
        else
        {
            float[] notes = Chords.GetChord(pattern[(int)enemy][currentChordI]);
            for (int i = 0; i < 3; ++i)
            {
                audio[i].Stop();
                audio[i].pitch = notes[i];
                audio[i].Play();

            }
            currentChordI++;
            return notes;
        }
    }
}
