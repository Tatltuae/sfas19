﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Chords
{
    private static int[] majorSemitones = new int[] { 2, 2, 1, 2, 2, 2, 1 };
    private static int[] minorSemitones = new int[] { 2, 1, 2, 2, 1, 2, 2 };

    public static float[] GetScale(int key, bool major)
    {
        float[] returnNotes = new float[7];
        int[] semitones = new int[7];
        if (major)
        {
            semitones = majorSemitones;
        }
        else
        {
            semitones = minorSemitones;
        }
        int pitch = -16 + key;
        for (int i = 0; i < 7; ++i)
        {
            returnNotes[i] = Mathf.Pow(2, (pitch) / 12.0f);
            pitch += semitones[i];
        }
        return returnNotes;
    }

    public static float[] GetChord(int chordNum)
    {
        float[] scale = GetScale(-3, true);
        chordNum--;
        return new float[] { scale[chordNum], scale[(chordNum + 2) % 7], scale[(chordNum + 4) % 7], scale[(chordNum + 6) % 7] };
    }
}
