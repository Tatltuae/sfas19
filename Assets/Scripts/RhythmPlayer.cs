﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class RhythmPlayer : MonoBehaviour
{
    [SerializeField]
    private float damage = 60.0f;
    [SerializeField]
    private float maxHealth = 100.0f;

    bool inCombat = false;
    bool checkEnemies = true;

    private float currentHealth;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        GameEvents.OnCombatInitiate += CombatInitiated;
        GameEvents.OnCombatEnd += CombatEnded;
    }

    // Update is called once per frame
    void Update()
    {
        if(!inCombat && Health <= 0)
        {
            SceneManager.LoadScene("RhythmScene");
        }
        if(checkEnemies && FindObjectsOfType<RhythmEnemy>().Length == 0)
        {
            StartCoroutine(DelayedScene());
        }
        else
        {
            checkEnemies = false;
        }
    }

    public float Damage { get { return damage; } }

    public float Health { get { return currentHealth; } }

    public float MaxHealth { get { return maxHealth; } }

    public void DamageHealth(float hit)
    {
        currentHealth -= hit;
        if(currentHealth < 0)
        {
            currentHealth = 0;
            // ~~ do death stuff
            // ~~ animate
        }
    }

    public void GiveHealth(float heal)
    {
        if (currentHealth + heal > maxHealth)
        {
            heal = maxHealth - currentHealth;
        }
        currentHealth += heal;
        // ~~ animate
    }

    public void CombatInitiated(RhythmPlayer player, RhythmEnemy enemy)
    {
        inCombat = true;
    }

    public void CombatEnded()
    {
        inCombat = false;
        checkEnemies = true;
    }

    IEnumerator DelayedScene()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("RhythmScene");
    }
}
