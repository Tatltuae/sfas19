﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // The Camera Target
    [SerializeField]
    Transform m_PlayerTransform;

    [SerializeField]
    Vector3 m_Difference;

    // The maximum speed of the camera
    [SerializeField]
    float m_maxSpeed = 2.0f;

    // Update is called once per frame
    void Update()
    {
        Vector3 target = m_PlayerTransform.position + m_Difference;
        Vector3 direction = target - transform.position;
        float dist = Vector3.Magnitude(direction);
        if((dist / Time.deltaTime) > m_maxSpeed)
        {
            direction = m_maxSpeed * Time.deltaTime * direction.normalized;
        }

        transform.Translate(direction);
    }
}
